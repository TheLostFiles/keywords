#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
	enum fields { word, hint, num_fields }; // starts an array 
	const int num_words = 10; // sets the amount of things in the array

	const string words[num_words][num_fields] = //array 
	{
		{"appendix", "Something in you feels like its going to burst"},
		{"temptation", "you really want to do it"},
		{"frequency", "how fast is that vibrating"},
		{"infrastructure", "I wonder how that's made"},
		{"hypothesis", "You need me to have ideas that could become reality"},
		{"paradox", "Something very strange in nature"},
		{"sausage", "You don't wanna know how I'm made"},
		{"literature", "To be or not to be that is the question"},
		{"supplementary", "Add me to complete or enhance"},
		{"ghostwriter", "Did they write this?"}
	};
    cout << "Welcome to KeyWords!\n\n";
	cout << "Unscramble the letters to make a word.\n";
	cout << "Enter 'hint' for a hint.\n";
	cout << "Enter 'quit' to quit.\n";
	int n = 0; // variable that sets the amount of times you play
	
	do // do
	{
		n++; // adds to the n variable 
		srand(static_cast<unsigned int>(time(0))); // sets the seed for the rand to the current time
		
        string guess; // guess 
	
	    int choice = rand() % num_words; // picks a random word
	
	    string the_word = words[choice][word]; // this makes it so i can use the variable
	    string the_hint = words[choice][hint];// this makes it so i can use the variable
		string jumble = the_word; // makes the variable
		int length = jumble.size();// sets the length of the jumbling to the size of the word
		
		for(int i = 0; i < length; ++i) // for loop basing off of the size of i 
		{
			int index1 = (rand() % length); // sets index1 to do a rand
			int index2 = (rand() % length); // sets index2 to do a rand
			char temp = jumble[index1]; // sets temp to index1
			jumble[index1] = jumble[index2]; // sets index1 to index2
			jumble[index2] = temp; // sets index2 to temp
		}
		
		
		cout << "The key word is: ";
		
		
		cout << jumble << "\n";
		cin >> guess; // puts in your guess
		
		while (guess != the_word && guess != "quit") // while for is input is quit 
		{
			if(guess == "hint") // checks if input is hint
			{
				cout << the_hint << "\n";
			}
			else
			{
				cout << "Sorry that's not it\n";
			}
			cout << "Your guess:";
			cin >> guess;
		}
		if(guess == the_word) // checks if the inout is the correct word
		{
		
			cout << "You got it! Good job!\n";
		}
	}
	while (n != 3);// makes it run 3 times
	
	cout << "Thanks for playing!\n";
	system("pause");
}